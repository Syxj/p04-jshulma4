package com.example.jorda.p04_jshulma4;

/**
 * Created by jorda on 3/7/2017.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Player {

    //player's image
    private Bitmap bitmap;

    //player's coordinates
    private int x;
    private int y;

    //Are we moving?
    private boolean boosted;

    //Give a value to constantly subtract, like the gravity in our Doodle Jump game
    private final int GRAVITY = -4;

    private final int MAX_DY = 25;
    private final int MIN_DY = -25;

    //screen height (width)
    private int maxY;
    private int minY;

    //motion speed
    private int dy = 0;

    //collision detection
    private Rect detectCollision;

    //constructor
    public Player(Context context, int screenX, int screenY)
    {
        x = 75;
        y = 75;
        dy = 1;

        //set image
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.helicopter);

        //get boundaries
        maxY = screenY - bitmap.getHeight();
        minY = 0;

        boosted = false;

        detectCollision = new Rect(x,y,bitmap.getWidth(),bitmap.getHeight());
    }

    public void setBoosted()
    {
        boosted = true;
    }

    public void stopBoosted()
    {
        boosted = false;
    }

    public void update()
    {
        if(boosted)
        {
            if(dy < -5) dy = -5;
            dy += 3;
        }
        else
        {
            dy += GRAVITY;
        }

        if(dy < MIN_DY)
        {
            dy = MIN_DY;
        }
        else if (dy > MAX_DY)
        {
            dy = MAX_DY;
        }

        y -= dy;

        if(y < minY)
        {
            y = minY;
        }
        else if(y > maxY)
        {
            y = maxY;
        }

        detectCollision.left = x;
        detectCollision.top = y;
        detectCollision.right = x + bitmap.getWidth();
        detectCollision.bottom = y + bitmap.getHeight();
    }

    public Rect getDetectCollision() { return detectCollision; }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDy() {
        return dy;
    }
}
