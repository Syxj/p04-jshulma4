package com.example.jorda.p04_jshulma4;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.view.MotionEvent;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by jorda on 3/7/2017.
 */

public class GameView extends SurfaceView implements Runnable {

    volatile boolean playing;

    //Scoring
    private int distance;
    private int highScore;

    //floor info
    private Bitmap floor;
    private Bitmap floorScaled;
    private int floorX;

    //game thread
    private Thread gameThread = null;

    private Player player;

    //Objects used for drawing
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;

    //Obstacle array
    private ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();

    //for making enemies
    private Context cont;
    private int screX;
    private int screY;

    //Class constructor
    public GameView(Context context, int screenX, int screenY)
    {
        super(context);


        screX = screenX;
        screY = screenY;
        cont = context;

        player = new Player(context, screenX, screenY);
        surfaceHolder = getHolder();
        paint = new Paint();

        distance = 0;
        highScore = 0;
        loadHighScore();
        System.out.println(highScore);

        floorX = 0;
        floor = BitmapFactory.decodeResource(context.getResources(), R.drawable.floor);
        floorScaled = Bitmap.createScaledBitmap(floor,screX,floor.getHeight(),true);
    }

    @Override
    public void run()
    {
        while(playing)
        {
            //Randomly add new obstacle
            addObstacle();

            //update the frame
            update();

            //draw the frame
            draw();

            //control
            control();

            //remove obstacles that are past the screen
            //removeObstacle();
        }
    }

    private void update()
    {
        //Update the player's location
        player.update();
        for(Obstacle obs : obstacles)
        {
            obs.update(player.getDy());
            if(Rect.intersects(player.getDetectCollision(),obs.getDetectCollision()))
            {
                gameOver();
                pause();
            }
        }
        distance++;
        floorX -= 20;
        if(Math.abs(floorX) > (floor.getWidth()/4)) floorX = -75;
    }

    private void draw()
    {
        if(surfaceHolder.getSurface().isValid())
        {
            canvas = surfaceHolder.lockCanvas();
            //will change to a background once we get the basis working
            canvas.drawColor(Color.DKGRAY);

            canvas.drawBitmap(
                    floor,
                    floorX,
                    screY-floor.getHeight(),
                    paint
            );

            //draw the player
            canvas.drawBitmap(
                    player.getBitmap(),
                    player.getX(),
                    player.getY(),
                    paint);

            //draw the obstacles
            for(Obstacle obs : obstacles)
            {

                canvas.drawBitmap(
                  obs.getBitmap(),
                        obs.getX(),
                        obs.getY(),
                        paint
                );
            }

            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    //We will update with roughly 60fps
    private void control()
    {
        try
        {
            gameThread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //don't want the game to update while we are paused
    public void pause()
    {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {/*nothing*/}
    }

    public void resume()
    {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent)
    {
        switch(motionEvent.getAction() & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_UP:
                //pressed
                player.stopBoosted();
                break;
            case MotionEvent.ACTION_DOWN:
                //let go
                if(playing) player.setBoosted();
                else
                {
                    player = new Player(getContext(),screX,screY);
                    obstacles = new ArrayList<Obstacle>();
                    distance = 0;
                    floorX = 0;
                    resume();
                };
                break;
        }
        return true;
    }

    public void addObstacle()
    {
        Random r = new Random();
        if(r.nextInt(100) > 95)
        {
            obstacles.add(new Obstacle(cont, screX, screY));
        }
    }

    public void removeObstacle()
    {
        for(Obstacle obs : obstacles)
        {
            if(obs.getX() < 1){
                obstacles.remove(obs);
            };
        }
    }

    public void gameOver()
    {
        if(distance > highScore)
        {
            highScore = distance;
            saveHighScore();
        }
        if(surfaceHolder.getSurface().isValid())
        {
            canvas = surfaceHolder.lockCanvas();

            canvas.drawColor(Color.WHITE);

            String gameOverStr = "Game Over";
            String finalScore = "You flew " + distance + " ft";
            String highScoreText = "High score is " + highScore + " ft";
            String restartGame = "Click the screen to restart";

            TextPaint textPaint = new TextPaint();
            textPaint.setAntiAlias(true);
            textPaint.setTextSize(32 * getResources().getDisplayMetrics().density);
            textPaint.setColor(0xFF000000);

            int width = (int) textPaint.measureText(gameOverStr);
            StaticLayout gameOverStrLayout = new StaticLayout(gameOverStr, textPaint, width, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0, false);
            canvas.translate((float)(screX/4),screY/10);
            gameOverStrLayout.draw(canvas);
            canvas.save();
            canvas.restore();

            width = (int) textPaint.measureText(finalScore);
            StaticLayout finalScoreLayout = new StaticLayout(finalScore, textPaint, width, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0, false);
            canvas.translate(0,screY/12);
            finalScoreLayout.draw(canvas);
            canvas.save();
            canvas.restore();

            width = (int) textPaint.measureText(highScoreText);
            StaticLayout hsLayout = new StaticLayout(highScoreText, textPaint, width, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0, false);
            canvas.translate(0,screY/14);
            hsLayout.draw(canvas);
            canvas.save();
            canvas.restore();

            width = (int) textPaint.measureText(restartGame);
            StaticLayout restartLayout = new StaticLayout(restartGame, textPaint, width, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0, false);
            canvas.translate(0,screY/14);
            restartLayout.draw(canvas);
            canvas.save();
            canvas.restore();

            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    //found via Android API and Stack Overflow
    private void loadHighScore()
    {
        File path = cont.getFilesDir();
        if(path.exists()) {
            File hsFile = new File(path, "highScore.txt");
            if (hsFile.exists()) {
                FileInputStream stream = null;
                try {
                    stream = new FileInputStream(hsFile);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                int length = (int) hsFile.length();
                byte[] bytes = new byte[length];
                try {
                    stream.read(bytes);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                String highScoreStr = new String(bytes);
                highScore = Integer.parseInt(highScoreStr);
            }
        }
    }


    //found via Android API and Stack Overflow
    private void saveHighScore() {
        File path = cont.getFilesDir();
        if(!path.mkdirs()) {
            System.out.println("Directory already exists");
        }
        File hsFile = new File(path,"highScore.txt");
        try {
            if(!hsFile.createNewFile()) {
                System.out.println("File already exists");
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(hsFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            stream.write(String.valueOf(distance).getBytes());
            System.out.println("High score saved!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
