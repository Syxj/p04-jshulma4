package com.example.jorda.p04_jshulma4;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.Random;

/**
 * Created by jorda on 3/8/2017.
 */

public class Obstacle {
    private Bitmap bitmap;
    private int x;
    private int y;

    private int dx = 10;

    private int minY;
    private int minX;

    private int maxY;
    private int maxX;

    private Rect detectCollision;

    public Obstacle(Context context, int screenX, int screenY)
    {
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.obstacle);
        maxX = screenX;
        maxY = screenY;
        minX = 0;
        minY = 0;

        x = screenX;

        Random generator = new Random();
        y = generator.nextInt(maxY) - bitmap.getHeight();

        detectCollision = new Rect(x,y,bitmap.getWidth(),bitmap.getHeight());
    }

    public void update(int playerSpeed)
    {
        x -= dx;

        //hit detection
        detectCollision.left = x;
        detectCollision.top = y;
        detectCollision.right = x + bitmap.getWidth();
        detectCollision.bottom = y + bitmap.getHeight();
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public Rect getDetectCollision() {
        return detectCollision;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDx() {
        return dx;
    }

    public int getMinY() {
        return minY;
    }

    public int getMinX() {
        return minX;
    }

    public int getMaxY() {
        return maxY;
    }

    public int getMaxX() {
        return maxX;
    }
}
